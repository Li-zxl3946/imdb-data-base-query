import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import javax.print.DocFlavor.URL;

enum sqlTableType
{
	UPDATE_MOVIE,
	UPDATE_MEMBER,
	UPDATE_GENRES,
	UPDATE_DIRECTOR,
	UPDATE_PRODUCER,
	UPDATE_ACTOR,
	UPDATE_WRITER,
	UPDATE_MOVIE_GENRES,
	UPDATE_ROLE,
	UPDATE_ACTOR_MOVIE_ROLE,
	SELECT_SQL,
	UPDATE_NONE
}

enum dataType
{
	SET_INT, 
	SET_STRING,
	SET_DOUBLE,
	SET_BOOLEAN,
	SET_YEAR
}

class sqlData
{
	public dataType type;
	public String attribute;
	public Object data;
	
	public sqlData(dataType type, String attribute, Object data)
	{
		this.type = type;
		this.attribute = attribute;
		this.data = data;
	}
}

class movieData
{
	public String movie_type;
	public String title;
	public String original_title;
	public boolean is_adult;
	public int start_year;
	public int end_year;
	public int run_time;
	public double avg_rating;
	public int num_vote;
}

class movieGenresData
{
	public int movieNameId;
	public int genresNameId;
	
	public movieGenresData(int movieNameId, int genresNameId)
	{
		this.movieNameId = movieNameId;
		this.genresNameId = genresNameId;
	}	
}

class memberData
{
	public int id;
	public String name;
	public int  birthYear;
	public int  deathYear;
	
	public memberData(int id, String name, int birthYear, int deathYear)
	{
		this.id = id;
		this.name = name;
		this.birthYear = birthYear;
		this.deathYear = deathYear; 
	}
}

class professionData
{
	public int movieNameId;
	public int memberNameId;
	
	public professionData(int movieNameId, int memberNameId)
	{
		this.movieNameId = movieNameId;
		this.memberNameId = memberNameId;
	}	
}

class actorMovieRoleData
{
	public int movieNameId;
	public int memberNameId;
	public int roleId;
	
	public actorMovieRoleData(int movieNameId, int memberNameId, int roleId)
	{
		this.movieNameId = movieNameId;
		this.memberNameId = memberNameId;
		this.roleId = roleId;
	}	
}

class roleData
{
	public int id;
	public String roleName;
	
	public roleData(int id, String roleName)
	{
		this.id = id;
		this.roleName = roleName;
	}
}

class SqlLoader implements Runnable
{
	private String jdbc_driver = "";
	private String db_url = "jdbc:mysql://localhost:3306/hw2?autoReconnect=true&useSSL=true";
	private String userName = "user1";
	private String password = "password1";
	private Connection db_connection = null;
	
	private int threadIndex;
	private sqlTableType tableType;
	private int numOfRecordToUpdate = 5000;
	
	private Map<Integer, movieData> movieDataList;
	private List<memberData> memberDataList;
	private List<String> genreDataList;
	private List<movieGenresData> movieGenresDataList;
	private List<professionData> writerDataList;
	private List<professionData> directorDataList;
	private List<professionData> producerDataList;
	private List<professionData> actorDataList;
	private List<roleData> roleDataList;
	private List<actorMovieRoleData> actorMovieRoleDataList;
	private int maxNumOfList;
	
	private String inputQuery;
	
	@Override
	public void run() 
	{
		// TODO Auto-generated method stub
        try
        {
        	this.connectDatabase();
        	
        	switch (this.tableType)
        	{
        	case UPDATE_MOVIE:
        		this.updateMovieDataToDataBase();
        		break;
        		
        	case UPDATE_MEMBER:
        		this.updateMemberDataToDataBase();
        		break;
        		
        	case UPDATE_GENRES:
        		this.updateGenresDataToDataBase();
        		break;
        		
        	case UPDATE_MOVIE_GENRES:
        		this.updateMovieGenresDataToDataBase();
        		break;
        		
        	case UPDATE_WRITER:
        		this.updateProfessionDataToDataBase("movie_writer", this.writerDataList);
        		this.writerDataList.clear();
        		break;
        		
        	case UPDATE_DIRECTOR:
        		this.updateProfessionDataToDataBase("movie_director", this.directorDataList);
        		this.directorDataList.clear();
        		break;
        		
        	case UPDATE_PRODUCER:
        		this.updateProfessionDataToDataBase("movie_producer", this.producerDataList);
        		this.producerDataList.clear();
        		break;

        	case UPDATE_ACTOR:
        		this.updateProfessionDataToDataBase("movie_actor", this.actorDataList);
        		this.actorDataList.clear();
        		break;

        	case UPDATE_ROLE:
        		this.updateRoleDataToDataBase();
        		this.roleDataList.clear();
        		break;
        		
        	case UPDATE_ACTOR_MOVIE_ROLE:
        		this.updateActorMovieRoleDataToDataBase();;
        		//this.actorMovieRoleDataList.clear();
        		break;
        		
        	case SELECT_SQL:
        		this.executeSelectSQL(this.inputQuery);
        		break;
        	}
        	
        	this.closeDatabase();
        }
        catch (Exception e)
        {
            // Throwing an exception
            System.out.println ("Exception is caught:" + e.getMessage());
        }
	}
	
	public void setUpdateDataType(sqlTableType tableType)
	{
		this.tableType = tableType;
	}
	
	public void connectDatabase()
	{
		try
		{
			Class.forName("com.mysql.jdbc.Driver");

		    // Open a connection
		    // System.out.print("Connecting to database...");
		    db_connection = DriverManager.getConnection(db_url, userName, password);
		    if (!db_connection.isClosed() || db_connection != null)
		    {
		    	// System.out.println(" Connected");
		    }
		    
			// Do not auto commit
			db_connection.setAutoCommit(false);
			
		}
		catch (ClassNotFoundException e)
		{
			e.printStackTrace();
		}
		catch(SQLException e)
		{
			// System.out.println(" Fail");
			
			// Catch Exception Close DataBase	
			try
			{
				if (db_connection != null)
				{
					db_connection.close();
				}
			}
			catch (SQLException e1) 
			{
				// TODO Auto-generated catch block
				e1.printStackTrace();
			};
		}
	}

	public void closeDatabase()
	{
	    try 
	    {
			if (!db_connection.isClosed() || db_connection != null)
			{
				db_connection.close();
			}
		} 
	    catch (SQLException e)
	    {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void commitSQL()
	{
		try
		{
			// Commit Statement
			db_connection.commit();
		}
		catch(SQLException e)
		{
			try 
			{
				if (db_connection != null)
				{
					db_connection.rollback();
				}
			}
			catch (SQLException e1) 
			{
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
	}

	public void setInputQuery(String query)
	{
		this.inputQuery = query;
	}
	
	public void executeSelectSQL(String SQLquery)
	{
		Statement sqlStmt = null;
		
		try
		{
			System.out.println("\n Query Result(Processing...):\n");
			
			// Prepare statement 
			sqlStmt = db_connection.createStatement();
		    ResultSet rs = sqlStmt.executeQuery(SQLquery);
		    ResultSetMetaData rsmd = rs.getMetaData();
		    int columnsNumber = rsmd.getColumnCount();
		    while (rs.next()) {
		        for (int i = 1; i <= columnsNumber; i++)
		        {
		            if (i > 1) System.out.print(" | ");
		            System.out.print(rs.getString(i));
		        }
		        System.out.println("");
		    }
		    
		    System.out.println("\n");
		}
		catch(SQLException e)
		{
			System.out.println(e.getMessage());
		}
		finally
		{
			try 
			{
				if (sqlStmt != null)
				{
					sqlStmt.close();
				}
			} 
			catch (SQLException e) 
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	public void executeSQL(String SQLquery, List<sqlData>tableRecord)
	{
		PreparedStatement sqlStmt = null;
		
		try
		{
			// Prepare statement 
			sqlStmt = db_connection.prepareStatement(SQLquery);
			for (int i = 0; i < tableRecord.size(); i++)
			{
				int curIndex = i + 1;
				
				if (tableRecord.get(i).data != null)
				{
					switch(tableRecord.get(i).type)
					{
					case SET_INT:
						sqlStmt.setInt(curIndex, (int)tableRecord.get(i).data);
						break;
					case SET_STRING:
						sqlStmt.setString(curIndex, (String)tableRecord.get(i).data);
						break;
					case SET_DOUBLE:
						sqlStmt.setDouble(curIndex, (double)tableRecord.get(i).data);
						break;
					case SET_YEAR:
						sqlStmt.setString(curIndex, (String)tableRecord.get(i).data);
						break;
					case SET_BOOLEAN:
						sqlStmt.setBoolean(curIndex, (boolean)tableRecord.get(i).data);
						break;
					}
				}
				else
				{
					switch(tableRecord.get(i).type)
					{
					case SET_INT:
						sqlStmt.setNull(curIndex, java.sql.Types.INTEGER);
						break;
					case SET_STRING:
						sqlStmt.setNull(curIndex, java.sql.Types.VARCHAR);
						break;
					case SET_DOUBLE:
						sqlStmt.setNull(curIndex, java.sql.Types.DOUBLE);
						break;
					case SET_YEAR:
						sqlStmt.setNull(curIndex, java.sql.Types.DATE);
						break;
					case SET_BOOLEAN:
						sqlStmt.setNull(curIndex, java.sql.Types.BOOLEAN);
						break;
					}
				}
			}
			sqlStmt.executeUpdate();
		}
		catch(SQLException e)
		{
			System.out.println(e.getMessage());
			System.out.println("Thread#" + this.threadIndex + ":SQL udpate error =>" + SQLquery);
			for (int i =0; i<tableRecord.size(); i++)
			{
				System.out.print(tableRecord.get(i).attribute + ":");
				System.out.println(tableRecord.get(i).data);
			}
			System.out.println();
		}
		finally
		{
			try 
			{
				if (sqlStmt != null)
				{
					sqlStmt.close();
				}
			} 
			catch (SQLException e) 
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
	}
	
	public void insertToTable(String tableName, List<sqlData>tableRecord)
	{			
		// Write Up a SQL String
		String sql = "INSERT INTO " + tableName;
		String sql_att = "(";
		String sql_data = "(";
		
		for (int i=0; i < tableRecord.size(); i++)
		{
			sql_att += tableRecord.get(i).attribute;
			sql_data += "?";
			
			if  (i == tableRecord.size()-1)
			{
				sql_att += ")";
				sql_data += ")";
			}
			else
			{
				sql_att += ", ";
				sql_data += ", ";
			}
		}
		
		sql = sql + sql_att + " VALUES" + sql_data;
			
		this.executeSQL(sql, tableRecord);
	}	

	public SqlLoader(int threadIndex)
	{
		this.threadIndex = threadIndex;
	}	

	public void setMemberDataList(List<memberData> memberDataList)
	{
		this.memberDataList = memberDataList;
	}
	
	public void updateMemberDataToDataBase()
	{
		int count = 0;
		
		for (int i = 0; i < this.memberDataList.size(); i++)
		{
			count++;
			
			List<sqlData>tableMap = new ArrayList<sqlData>();
			
			tableMap.add(new sqlData(dataType.SET_INT, "id", this.memberDataList.get(i).id));
			tableMap.add(new sqlData(dataType.SET_STRING, "member_name", this.memberDataList.get(i).name));
			
			if (this.memberDataList.get(i).birthYear == -1)
			{
				tableMap.add(new sqlData(dataType.SET_INT, "birth_year", null));
			}
			else
			{
				tableMap.add(new sqlData(dataType.SET_INT, "birth_year", this.memberDataList.get(i).birthYear));
			}
			
			if (this.memberDataList.get(i).deathYear == -1)
			{
				tableMap.add(new sqlData(dataType.SET_INT, "death_year", null));
			}
			else
			{
				tableMap.add(new sqlData(dataType.SET_INT, "death_year", this.memberDataList.get(i).deathYear));
			}
			
			this.insertToTable("member", tableMap);
			
			if (count % this.numOfRecordToUpdate == 0)
			{
				commitSQL();
			}

		}
		
		commitSQL();
		
		this.memberDataList.clear();
	}
	
	public void setMovieDataList(Map<Integer, movieData> movieDataList)
	{
		this.movieDataList = movieDataList;
	}

	public void updateMovieDataToDataBase()
	{
		int count =0;
				
		// Loop through the Movie Data list
		Iterator it = this.movieDataList.entrySet().iterator();
		while (it.hasNext())
		{
			count++;
			Map.Entry<Integer, movieData> pair = (Map.Entry<Integer, movieData>) it.next();
			
			List<sqlData>tableMap = new ArrayList<sqlData>();
			
			tableMap.add(new sqlData(dataType.SET_INT, "id", pair.getKey()));
			
			if (pair.getValue().movie_type.equals("\\N"))
			{
				tableMap.add(new sqlData(dataType.SET_STRING, "movie_type", null));
			}
			else
			{
				tableMap.add(new sqlData(dataType.SET_STRING, "movie_type", pair.getValue().movie_type));
			}
			
			if (pair.getValue().title.equals("\\N"))
			{
				tableMap.add(new sqlData(dataType.SET_STRING, "title", null));
			}
			else
			{
				tableMap.add(new sqlData(dataType.SET_STRING, "title", pair.getValue().title));
			}
			
			if (pair.getValue().original_title.equals("\\N"))
			{
				tableMap.add(new sqlData(dataType.SET_STRING, "original_title", null));
			}
			else
			{
				tableMap.add(new sqlData(dataType.SET_STRING, "original_title", pair.getValue().original_title));
			}
			
			tableMap.add(new sqlData(dataType.SET_BOOLEAN, "is_adult", pair.getValue().is_adult));
			
			if (pair.getValue().start_year == -1)
			{
				tableMap.add(new sqlData(dataType.SET_INT, "start_year", null));
			}
			else
			{
				tableMap.add(new sqlData(dataType.SET_INT, "start_year", pair.getValue().start_year));
			}
			
			if (pair.getValue().end_year == -1)
			{
				tableMap.add(new sqlData(dataType.SET_INT, "end_year", null));
			}
			else
			{
				tableMap.add(new sqlData(dataType.SET_INT, "end_year", pair.getValue().end_year));
			}
			
			if (pair.getValue().run_time == -1)
			{
				tableMap.add(new sqlData(dataType.SET_INT, "run_time", null));
			}
			else
			{
				tableMap.add(new sqlData(dataType.SET_INT, "run_time", pair.getValue().run_time));
			}
			
			if (pair.getValue().avg_rating == -1)
			{
				tableMap.add(new sqlData(dataType.SET_DOUBLE, "avg_rating", null));
			}
			else
			{
				tableMap.add(new sqlData(dataType.SET_DOUBLE, "avg_rating", pair.getValue().avg_rating));
			}
			
			if (pair.getValue().num_vote == -1)
			{
				tableMap.add(new sqlData(dataType.SET_INT, "num_vote", null));
			}
			else
			{
				tableMap.add(new sqlData(dataType.SET_INT, "num_vote", pair.getValue().num_vote));
			}
			
			this.insertToTable("movie", tableMap);
			it.remove();
			
			if (count % this.numOfRecordToUpdate == 0)
			{
				commitSQL();
			}

		}
		
		commitSQL();
		
		this.movieDataList.clear();
	}

	public void setGenreDataList(List<String> genreList)
	{
		this.genreDataList = genreList;
	}

	public void updateGenresDataToDataBase()
	{
		int count = 0;
		for(int i=0; i<this.genreDataList.size(); i++)
		{
			count++;
			List<sqlData>tableMap = new ArrayList<sqlData>();
			tableMap.add(new sqlData(dataType.SET_INT, "id", (i+1)));
			tableMap.add(new sqlData(dataType.SET_STRING, "genre_name", this.genreDataList.get(i)));
			
			this.insertToTable("genre", tableMap);
		}
		
		commitSQL();
		
		this.genreDataList.clear();
	}

	public void setMovieGenresDataList(List<movieGenresData> movieGenresDataList)
	{
		this.movieGenresDataList = movieGenresDataList;
	}

	public void updateMovieGenresDataToDataBase()
	{
		int count = 0;
		for(int i=0; i<this.movieGenresDataList.size(); i++)
		{
			count++;
			
			List<sqlData>tableMap = new ArrayList<sqlData>();
			tableMap.add(new sqlData(dataType.SET_INT, "movie_id", movieGenresDataList.get(i).movieNameId));
			tableMap.add(new sqlData(dataType.SET_INT, "genre_id", movieGenresDataList.get(i).genresNameId));
			
			this.insertToTable("movie_genre", tableMap);
			
			if (count % this.numOfRecordToUpdate == 0)
			{
				commitSQL();
			}

		}
		
		commitSQL();
		
		this.movieGenresDataList.clear();
	}

	public void setWriterDataList(List<professionData> writerDataList)
	{
		this.writerDataList = writerDataList;
	}

	public void setDirectorDataList(List<professionData> directorDataList)
	{
		this.directorDataList = directorDataList;
	}
	
	public void setProducerDataList(List<professionData> producerDataList)
	{
		this.producerDataList = producerDataList;
	}
	
	public void setActorDataList(List<professionData> actorDataList)
	{
		this.actorDataList = actorDataList;
	}	
	
	public void updateProfessionDataToDataBase(String tableName, List<professionData> professionDataList)
	{
		int count = 0;
		int missingIDCount = 0;
		
		for (int i = 0; i < professionDataList.size(); i++)
		{
			if (	!isIdExist("member", professionDataList.get(i).memberNameId) ||
					!isIdExist("movie", professionDataList.get(i).movieNameId)	)
			{
				missingIDCount++;
				continue;
			}
			
			count++;
			List<sqlData>tableMap = new ArrayList<sqlData>();

			tableMap.add(new sqlData(dataType.SET_INT, "member_id", professionDataList.get(i).memberNameId));
			tableMap.add(new sqlData(dataType.SET_INT, "movie_id", professionDataList.get(i).movieNameId));
			
			this.insertToTable(tableName, tableMap);
						
			if (count % this.numOfRecordToUpdate == 0)
			{
				commitSQL();
			}
		}
		
		commitSQL();
	}
	
	public void setRoleData(List<roleData> roleDataList)
	{
		this.roleDataList = roleDataList;
	}
	
	public void updateRoleDataToDataBase()
	{
		int count = 0;
		int missingIDCount = 0;
		
		for (int i = 0; i < this.roleDataList.size(); i++)
		{			
			count++;
			List<sqlData>tableMap = new ArrayList<sqlData>();

			tableMap.add(new sqlData(dataType.SET_INT, "id",  this.roleDataList.get(i).id));
			tableMap.add(new sqlData(dataType.SET_STRING, "role_name",  this.roleDataList.get(i).roleName));
			
			this.insertToTable("role", tableMap);
						
			if (count % this.numOfRecordToUpdate == 0)
			{
				commitSQL();
			}
		}
		
		commitSQL();
	}
	
	public void setActorMovieRoleDataList(List<actorMovieRoleData> actorMovieRoleDataList)
	{
		this.actorMovieRoleDataList = actorMovieRoleDataList;
	}

	public void updateActorMovieRoleDataToDataBase()
	{
		int count = 0;
		int missingIDCount = 0;
				
		for (int i = 0; i < this.actorMovieRoleDataList.size(); i++)
		{			
			count++;
			List<sqlData>tableMap = new ArrayList<sqlData>();

			tableMap.add(new sqlData(dataType.SET_INT, "member_id",  this.actorMovieRoleDataList.get(i).memberNameId));
			tableMap.add(new sqlData(dataType.SET_INT, "movie_id",  this.actorMovieRoleDataList.get(i).movieNameId));
			tableMap.add(new sqlData(dataType.SET_INT, "role_id",  this.actorMovieRoleDataList.get(i).roleId));
			
			this.insertToTable("actor_movie_role", tableMap);
						
			if (count % this.numOfRecordToUpdate == 0)
			{
				commitSQL();
			}
		}
		
		commitSQL();
	}	
	
	public boolean isIdExist(String tableName, int id)
	{
		boolean rtnVal = false;
		
		String SQLquery = "select * from " + tableName + " where id=" + id + ";";
		
		Statement sqlStmt = null;
		
		try
		{
			// Prepare statement 
			sqlStmt = db_connection.createStatement();
			ResultSet rs = sqlStmt.executeQuery(SQLquery);
			rtnVal = rs.next();
						
		}
		catch(SQLException e)
		{
			System.out.println("SQL select error:" + SQLquery);
		}
		finally
		{
			try 
			{
				if (sqlStmt != null)
				{
					sqlStmt.close();
				}
			} 
			catch (SQLException e) 
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		
		return rtnVal;
	}

	public void setMaxNumOfList(int maxNumOfList)
	{
		this.maxNumOfList = maxNumOfList;
	}
}

public class SqlEngine 
{
	private String jdbc_driver = "";
	private String db_url = "jdbc:mysql://localhost:3306/hw2?autoReconnect=true&useSSL=true";
	private String userName = "user1";
	private String password = "password1";
	private Connection db_connection = null;
	private long startTime; 
	private int numOfThread;
	
	private List<Map<Integer, movieData>> movieDataList;
	private List<ArrayList<movieGenresData>> movieGenresDataList;
	private List<String> genresList;
	
	private List<ArrayList<memberData>> memberDataList;
	private List<ArrayList<professionData>> actorDataList;
	private int actorMaxSize;
	private List<ArrayList<professionData>> directorDataList;
	private List<ArrayList<professionData>> writerDataList;
	private List<ArrayList<professionData>> producerDataList;
	
	private List<ArrayList<roleData>> roleDataList;
	private List<ArrayList<actorMovieRoleData>> actorMovieRoleDataList;
	
	public SqlEngine()
	{
		numOfThread = 25;
		this.movieDataList = new ArrayList<Map<Integer, movieData>>();
		this.movieGenresDataList = new ArrayList<ArrayList<movieGenresData>>();
		this.genresList = new ArrayList<String>();	
		
		this.memberDataList = new ArrayList<ArrayList<memberData>>();
		this.actorDataList = new ArrayList<ArrayList<professionData>>();
		this.directorDataList = new ArrayList<ArrayList<professionData>>();
		this.writerDataList =  new ArrayList<ArrayList<professionData>>();
		this.producerDataList = new ArrayList<ArrayList<professionData>>();
		
		this.roleDataList = new ArrayList<ArrayList<roleData>>();
		this.actorMovieRoleDataList = new ArrayList<ArrayList<actorMovieRoleData>>();
	}
	
	public ArrayList<ArrayList<professionData>> professionDataPartition(List<professionData> inputList)
	{
		ArrayList<ArrayList<professionData>> rtnList = new ArrayList<ArrayList<professionData>>();
		
		int maxNumOfRecord = (int) Math.ceil((double)inputList.size()/this.numOfThread);
		for (int i = 0; i < inputList.size(); i++)
		{
			if( i % maxNumOfRecord ==0)
			{
				rtnList.add(new ArrayList<professionData>());
			}
			
			rtnList.get((i / maxNumOfRecord)).add(inputList.get(i));
		}
		
		return rtnList;
	}
	
	public void loadActorData()
	{
		// Read the content
		try 
		{
			System.out.print("Load title_principals_tsv_gz/data.tsv file... ");
			File file = new File("title_principals_tsv_gz/data.tsv");
			BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(file), "UTF8"));
			String st;
			
			int lineCount = 0;
			int movieId;
			int nameId;			
			
			Set<String> tmpActorSet = new HashSet<String>();
			List<professionData> tmpActorList = new ArrayList<professionData>();
			
			Set<String> tmpRoleSet = new HashSet<String>();
			Map<String, Integer> tmpRoleMap = new HashMap<String, Integer>();
			List<roleData> tmpRoleList = new ArrayList<roleData>();
			List<actorMovieRoleData> tmpActorMovieRoleList = new ArrayList<actorMovieRoleData>();
			
			st = br.readLine();
			
			while ((st = br.readLine()) != null)
			{
				lineCount++;
				
				String[] str_array = st.split("\t");

				movieId = Integer.parseInt(str_array[0].substring(2));
				nameId = Integer.parseInt(str_array[2].substring(2));
				String tmpMemberStr = Integer.toString(movieId) + " " + Integer.toString(nameId);
				
				if(str_array[3].equals("actor") || str_array[3].equals("actress"))
				{
					if (!tmpActorSet.contains(tmpMemberStr))
					{
						tmpActorSet.add(tmpMemberStr);
						
						tmpActorList.add(new professionData(movieId, nameId));
						if(!str_array[5].equals("\\N"))
						{
							String[] roleStr = str_array[5].split("\",\"");
							for (int i = 0; i < roleStr.length; i++)
							{
								roleStr[i] = roleStr[i].replaceAll("[\\[\"\\]]", "");
								if (roleStr[i].contains("Various including"))
								{
									continue;
								}
								
								if (!tmpRoleSet.contains(roleStr[i]))
								{
									tmpRoleSet.add(roleStr[i]);
									tmpRoleMap.put(roleStr[i], tmpRoleSet.size());
									tmpRoleList.add(new roleData(tmpRoleSet.size(), roleStr[i]));
								}
						
								tmpActorMovieRoleList.add(new actorMovieRoleData(movieId, nameId, tmpRoleMap.get(roleStr[i])));
							}
						}
					}
				}
				
			}
			
			tmpActorSet.clear();
			tmpRoleMap.clear();
			
			this.actorDataList = professionDataPartition(tmpActorList);
			
			int maxNumOfRecord = (int) Math.ceil((double)tmpRoleList.size()/this.numOfThread);
			for (int i = 0; i < tmpRoleList.size(); i++)
			{
				if( i % maxNumOfRecord ==0)
				{
					this.roleDataList.add(new ArrayList<roleData>());
				}
				
				this.roleDataList.get((i / maxNumOfRecord)).add(tmpRoleList.get(i));
			}
			
			maxNumOfRecord = (int) Math.ceil((double)tmpActorMovieRoleList.size()/this.numOfThread);
			for (int i = 0; i < tmpActorMovieRoleList.size(); i++)
			{
				if( i % maxNumOfRecord ==0)
				{
					this.actorMovieRoleDataList.add(new ArrayList<actorMovieRoleData>());
				}
				
				this.actorMovieRoleDataList.get((i / maxNumOfRecord)).add(tmpActorMovieRoleList.get(i));
			}
			
			this.actorMaxSize = (int) Math.ceil((double)tmpActorList.size()/this.numOfThread);
			
			System.out.println("Done (" + 
						"Actor size:" + tmpActorList.size() +
						", Role Size:" + tmpRoleList.size() +
						", Actor Movie Role Size:" + tmpActorMovieRoleList.size() +
						")"
			);
		}
		catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void loadProfessionData()
	{	
		// Read the content
		try 
		{
			System.out.print("Load title_principals_tsv_gz/data.tsv file... ");
			File file = new File("title_principals_tsv_gz/data.tsv");
			BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(file), "UTF8"));
			String st;
			
			int lineCount = 0;
			int movieId;
			int nameId;			
			
			Set<String> tmpDirectorSet = new HashSet<String>();
			Set<String> tmpProducerSet = new HashSet<String>();
			Set<String> tmpWriterSet = new HashSet<String>();
			
			List<professionData> tmpDirectorList = new ArrayList<professionData>();
			List<professionData> tmpProducerList = new ArrayList<professionData>();
			List<professionData> tmpWriterList = new ArrayList<professionData>();
						
			st = br.readLine();
			
			while ((st = br.readLine()) != null)
			{
				lineCount++;
				
				String[] str_array = st.split("\t");

				movieId = Integer.parseInt(str_array[0].substring(2));
				nameId = Integer.parseInt(str_array[2].substring(2));
				String tmpMemberStr = Integer.toString(movieId) + " " + Integer.toString(nameId);
				
				if (str_array[3].equals("director"))
				{
					if (!tmpDirectorSet.contains(tmpMemberStr))
					{
						tmpDirectorSet.add(tmpMemberStr);
						tmpDirectorList.add(new professionData(movieId, nameId));
					}
				}
				else if(str_array[3].equals("producer"))
				{
					if (!tmpProducerSet.contains(tmpMemberStr))
					{
						tmpProducerSet.add(tmpMemberStr);
						tmpProducerList.add(new professionData(movieId, nameId));
					}
				}
				else if(str_array[3].equals("writer"))
				{
					if (!tmpWriterSet.contains(tmpMemberStr))
					{
						tmpWriterSet.add(tmpMemberStr);
						tmpWriterList.add(new professionData(movieId, nameId));
					}
				}
			}
			
			tmpDirectorSet.clear();
			tmpProducerSet.clear();
			tmpWriterSet.clear();
			
			this.directorDataList = professionDataPartition(tmpDirectorList);
			this.producerDataList = professionDataPartition(tmpProducerList);
			this.writerDataList = professionDataPartition(tmpWriterList);
						
			System.out.println("Done (" + 
						"Director size:" + tmpDirectorList.size() +
						", Producer size:" + tmpProducerList.size() +
						", Writer size:" + tmpWriterList.size() +
						")"
			);
		}
		catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void loadMemberData()
	{
		// Read Name Data		
		// Read the content
		try 
		{
			System.out.print("Load name_basics_tsv_gz/data.tsv file... ");
			File file = new File("name_basics_tsv_gz/data.tsv");
			BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(file), "UTF8"));
			String st;
			
			List<memberData> tmpRecordList = new ArrayList<memberData>();
			int lineCount = 0;
			st = br.readLine();
			
			while ((st = br.readLine()) != null)
			{
				lineCount++;
				String[] str_array = st.split("\t");
				
				memberData tmpRecord = new memberData(
						Integer.parseInt(str_array[0].substring(2)), 
						str_array[1], 
						Integer.parseInt(str_array[2].equals("\\N")?"-1":str_array[2]),
						Integer.parseInt(str_array[3].equals("\\N")?"-1":str_array[3])
				);
								
				tmpRecordList.add(tmpRecord);
			}
			
			int maxNumOfRecord = (int) Math.ceil((double)tmpRecordList.size()/this.numOfThread);
			
			for (int i = 0; i < tmpRecordList.size(); i++)
			{
				if( i % maxNumOfRecord ==0)
				{
					this.memberDataList.add(new ArrayList<memberData>());
				}
				
				this.memberDataList.get((i / maxNumOfRecord)).add(tmpRecordList.get(i));
			}
			
			System.out.println("Done (Member Data Size:" + tmpRecordList.size() + ")");
		}
		catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void updateMemberData()
	{
		this.startTimer("Importing Member Data to SQL Database... ");
		List<Thread> threadList= new ArrayList<Thread>();
		
		for(int i =0; i < this.numOfThread; i++)
		{
			SqlLoader tmpSqlLoader = new SqlLoader(i);
			tmpSqlLoader.setMemberDataList(this.memberDataList.get(i));
			tmpSqlLoader.setUpdateDataType(sqlTableType.UPDATE_MEMBER);
			threadList.add(new Thread(tmpSqlLoader));
		}
		
		this.memberDataList.clear();
		
		for(int i =0; i < numOfThread; i++)
		{
			threadList.get(i).start();
		}
		
		for(int i =0; i < numOfThread; i++)
		{
			try 
			{
				threadList.get(i).join();
			} 
			catch (InterruptedException e) 
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		this.stopTimer();
	}
	
	public void loadGenresData()
	{
		try 
		{
			System.out.print("Load title_basics_tsv_gz/data.tsv file...");
			// Read Movie Data
			File file = new File("title_basics_tsv_gz/data.tsv");
			BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(file), "UTF8"));
			String st;
			
			int lineCount = 0;
			
			// Skip the first line
			st = br.readLine();
			
			List<String> tmpGenresList = new ArrayList<String>();
			List<movieGenresData> tmpMovieGenresList = new ArrayList<movieGenresData>();
			
			// Read the content
			while ((st = br.readLine()) != null)
			{
			    String[] str_array = st.split("\t");
			    
			    String genres = str_array[8];
			    String[] genres_token_list = genres.split(",");
			    int movieId = Integer.parseInt(str_array[0].substring(2));
			    
			    for (String token: genres_token_list )
			    {
			    	lineCount++;
			    	if (!tmpGenresList.contains(token))
			    	{
			    		tmpGenresList.add(token);
			    	}
			    	
			    	tmpMovieGenresList.add(new movieGenresData(movieId, (tmpGenresList.indexOf(token)+1)));
			    }
			}
			
			this.movieGenresDataList = new ArrayList<ArrayList<movieGenresData>>();
			
			int maxNumOfRecord = (int) Math.ceil((double)tmpMovieGenresList.size()/this.numOfThread);
			for (int i = 0; i < tmpMovieGenresList.size(); i++)
			{
				if( i % maxNumOfRecord ==0)
				{
					movieGenresDataList.add(new ArrayList<movieGenresData>());
				}
				
				movieGenresDataList.get((i / maxNumOfRecord)).add(tmpMovieGenresList.get(i));
			}
			
			this.genresList = tmpGenresList;
			
			System.out.println(
					"Done (total genres:" + this.genresList.size() + 
					", total movie_genres:" + tmpMovieGenresList.size() + ")");
		}
		catch (IOException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void updateGenresData()
	{
		this.startTimer("Importing Genres Data to SQL Database... ");
		
		SqlLoader tmpSqlLoader = new SqlLoader(0);
		tmpSqlLoader.setGenreDataList(this.genresList);;
		tmpSqlLoader.setUpdateDataType(sqlTableType.UPDATE_GENRES);
		Thread tmpThread = new Thread(tmpSqlLoader);
		
		try 
		{
			tmpThread.start();
			tmpThread.join();
		} 
		catch (InterruptedException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		this.stopTimer();
	}

	public void updateMovieGenresData()
	{
		this.startTimer("Importing Movie Genres Data to SQL Database... ");
		List<Thread> threadList= new ArrayList<Thread>();
		
		for(int i =0; i < this.numOfThread; i++)
		{
			SqlLoader tmpSqlLoader = new SqlLoader(i);
			tmpSqlLoader.setMovieGenresDataList(this.movieGenresDataList.get(i));
			tmpSqlLoader.setUpdateDataType(sqlTableType.UPDATE_MOVIE_GENRES);
			threadList.add(new Thread(tmpSqlLoader));
		}
		
		this.movieGenresDataList.clear();
		
		for(int i =0; i < numOfThread; i++)
		{
			threadList.get(i).start();
		}
		
		for(int i =0; i < numOfThread; i++)
		{
			try 
			{
				threadList.get(i).join();
			} 
			catch (InterruptedException e) 
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		this.stopTimer();
	}
	
	public void loadMovieData()
	{
		try 
		{
			System.out.print("Load title_basics_tsv_gz/data.tsv file...");
			Map<Integer, movieData> tmpList = new<Integer, movieData>HashMap();
			
			// Read Movie Data
			File file = new File("title_basics_tsv_gz/data.tsv");
			BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(file), "UTF8"));
			String st;
			
			int lineCount = 0;
			
			// Skip the first line
			st = br.readLine();
			
			// Read the content
			while ((st = br.readLine()) != null)
			{
				lineCount++;
			    String[] str_array = st.split("\t");
			    			   
			    movieData tmpData = new movieData();
			    tmpData.movie_type = new String(str_array[1]);
			    tmpData.title = new String(str_array[2]);
			    tmpData.original_title = new String(str_array[3]);
			    tmpData.is_adult = str_array[4].equals("0")?false:true;
			    tmpData.start_year = Integer.parseInt(str_array[5].equals("\\N")?"-1":str_array[5]);
			    tmpData.end_year = Integer.parseInt(str_array[6].equals("\\N")?"-1":str_array[6]);
			    tmpData.run_time = Integer.parseInt(str_array[7].equals("\\N")?"-1":str_array[7]);
			    tmpData.avg_rating = -1.0;
			    tmpData.num_vote = -1;
			    
			    tmpList.put(Integer.parseInt(str_array[0].substring(2)), tmpData);
			    
			    /*
			    if (lineCount > 10)
			    {
			    	break;
			    }
			    else
			    {
			    	System.out.print("\n" + st);
			    }*/
			}
			
			System.out.println("Done (Number of Records:" + tmpList.size() + ")");
			
			System.out.print("Load title_rating_tsv_gz/data.tsv file...");
			// read vote and rating
			file = new File("title_rating_tsv_gz/data.tsv");
			br = new BufferedReader(new InputStreamReader(new FileInputStream(file), "UTF8"));
			
			// Skip the first line
			st = br.readLine();
			
			// Read the content
			while ((st = br.readLine()) != null)
			{
				String[] str_array = st.split("\t");
				tmpList.get(Integer.parseInt(str_array[0].substring(2))).avg_rating = Double.parseDouble(str_array[1]);
				tmpList.get(Integer.parseInt(str_array[0].substring(2))).num_vote = Integer.parseInt(str_array[2]);
			}
			
			int maxNumOfRecord = (int) Math.ceil((double)tmpList.size()/this.numOfThread);
			int numOfRecord = 0;
			
			// Loop through the Movie Data list
			Iterator it = tmpList.entrySet().iterator();
			Map<Integer, movieData> tokenList = new<Integer, movieData>HashMap();
			
			while (it.hasNext())
			{
				numOfRecord++;
				Map.Entry<Integer, movieData> pair = (Map.Entry<Integer, movieData>) it.next();
				tokenList.put(pair.getKey(), pair.getValue());
				
				if (numOfRecord > maxNumOfRecord)
				{
					this.movieDataList.add(tokenList);
					tokenList = new<Integer, movieData>HashMap();
					numOfRecord = 0;
				}
				else if(it.hasNext() == false)
				{
					this.movieDataList.add(tokenList);
				}
			}
			
			System.out.println("Done");
		}
		catch (IOException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
	}
	
	public void updateMovieData()
	{
		this.startTimer("Importing Movie Data to SQL Database... ");
		List<Thread> threadList= new ArrayList<Thread>();
		
		for(int i =0; i < this.numOfThread; i++)
		{
			SqlLoader tmpSqlLoader = new SqlLoader(i);
			tmpSqlLoader.setMovieDataList(this.movieDataList.get(i));
			tmpSqlLoader.setUpdateDataType(sqlTableType.UPDATE_MOVIE);
			threadList.add(new Thread(tmpSqlLoader));
		}
		
		this.movieDataList.clear();
		
		for(int i =0; i < numOfThread; i++)
		{
			threadList.get(i).start();
		}
		
		for(int i =0; i < numOfThread; i++)
		{
			try 
			{
				threadList.get(i).join();
			} 
			catch (InterruptedException e) 
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		this.stopTimer();
	}
	
	public void updateMovieWriterData()
	{
		this.startTimer("Importing Movie Writer Data to SQL Database... ");
		List<Thread> threadList= new ArrayList<Thread>();
		
		for(int i =0; i < this.numOfThread; i++)
		{
			SqlLoader tmpSqlLoader = new SqlLoader(i);
			tmpSqlLoader.setWriterDataList(this.writerDataList.get(i));
			tmpSqlLoader.setUpdateDataType(sqlTableType.UPDATE_WRITER);
			threadList.add(new Thread(tmpSqlLoader));
		}
		
		this.writerDataList.clear();
		
		for(int i =0; i < numOfThread; i++)
		{
			threadList.get(i).start();
		}
		
		for(int i =0; i < numOfThread; i++)
		{
			try 
			{
				threadList.get(i).join();
			} 
			catch (InterruptedException e) 
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		this.stopTimer();
	}

	public void updateMovieDirectorData()
	{
		this.startTimer("Importing Movie Director Data to SQL Database... ");
		List<Thread> threadList= new ArrayList<Thread>();
		
		for(int i =0; i < this.numOfThread; i++)
		{
			SqlLoader tmpSqlLoader = new SqlLoader(i);
			tmpSqlLoader.setDirectorDataList(this.directorDataList.get(i));
			tmpSqlLoader.setUpdateDataType(sqlTableType.UPDATE_DIRECTOR);
			threadList.add(new Thread(tmpSqlLoader));
		}
		
		this.directorDataList.clear();
		
		for(int i =0; i < numOfThread; i++)
		{
			threadList.get(i).start();
		}
		
		for(int i =0; i < numOfThread; i++)
		{
			try 
			{
				threadList.get(i).join();
			} 
			catch (InterruptedException e) 
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		this.stopTimer();
	}
	
	public void updateMovieProducerData()
	{
		this.startTimer("Importing Movie Producer Data to SQL Database... ");
		List<Thread> threadList= new ArrayList<Thread>();
		
		for(int i =0; i < this.numOfThread; i++)
		{
			SqlLoader tmpSqlLoader = new SqlLoader(i);
			tmpSqlLoader.setProducerDataList(this.producerDataList.get(i));
			tmpSqlLoader.setUpdateDataType(sqlTableType.UPDATE_PRODUCER);
			threadList.add(new Thread(tmpSqlLoader));
		}
		
		this.producerDataList.clear();
		
		for(int i =0; i < numOfThread; i++)
		{
			threadList.get(i).start();
		}
		
		for(int i =0; i < numOfThread; i++)
		{
			try 
			{
				threadList.get(i).join();
			} 
			catch (InterruptedException e) 
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		this.stopTimer();
	}
	
	public void updateMovieActorData()
	{
		this.startTimer("Importing Movie Actor Data to SQL Database... ");
		List<Thread> threadList= new ArrayList<Thread>();
		
		for(int i =0; i < this.numOfThread; i++)
		{
			SqlLoader tmpSqlLoader = new SqlLoader(i);
			tmpSqlLoader.setActorDataList(this.actorDataList.get(i));
			tmpSqlLoader.setMaxNumOfList(this.actorMaxSize);
			tmpSqlLoader.setUpdateDataType(sqlTableType.UPDATE_ACTOR);
			threadList.add(new Thread(tmpSqlLoader));
		}
		
		this.actorDataList.clear();
		
		for(int i =0; i < numOfThread; i++)
		{
			threadList.get(i).start();
		}
		
		for(int i =0; i < numOfThread; i++)
		{
			try 
			{
				threadList.get(i).join();
			} 
			catch (InterruptedException e) 
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		this.stopTimer();
	}
	
	public void updateRoleData()
	{
		this.startTimer("Importing Role Data to SQL Database... ");
		List<Thread> threadList= new ArrayList<Thread>();
		
		for(int i =0; i < this.numOfThread; i++)
		{
			SqlLoader tmpSqlLoader = new SqlLoader(i);
			tmpSqlLoader.setRoleData(this.roleDataList.get(i));
			tmpSqlLoader.setUpdateDataType(sqlTableType.UPDATE_ROLE);
			threadList.add(new Thread(tmpSqlLoader));
		}
		
		this.roleDataList.clear();
		
		for(int i =0; i < numOfThread; i++)
		{
			threadList.get(i).start();
		}
		
		for(int i =0; i < numOfThread; i++)
		{
			try 
			{
				threadList.get(i).join();
			} 
			catch (InterruptedException e) 
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		this.stopTimer();
	}
	
	public void updateActorMovieRoleData()
	{
		this.startTimer("Importing Actor Movie Role Data to SQL Database... ");
		List<Thread> threadList= new ArrayList<Thread>();
				
		for(int i =0; i < this.numOfThread; i++)
		{
			SqlLoader tmpSqlLoader = new SqlLoader(i);
			tmpSqlLoader.setActorMovieRoleDataList(this.actorMovieRoleDataList.get(i));
			tmpSqlLoader.setUpdateDataType(sqlTableType.UPDATE_ACTOR_MOVIE_ROLE);
			threadList.add(new Thread(tmpSqlLoader));
		}
		
		this.actorMovieRoleDataList.clear();
		
		for(int i =0; i < numOfThread; i++)
		{
			threadList.get(i).start();
		}
		
		for(int i =0; i < numOfThread; i++)
		{
			try 
			{
				threadList.get(i).join();
			} 
			catch (InterruptedException e) 
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		this.stopTimer();
	}
	
	public void updateDataToDataBase()
	{
		this.loadMovieData();
		this.updateMovieData();
		
		this.loadMemberData();
		this.updateMemberData();
	
		this.loadGenresData();
		this.updateGenresData();
		this.updateMovieGenresData();

		this.loadProfessionData();
		this.updateMovieWriterData();
		this.updateMovieDirectorData();
		this.updateMovieProducerData();	
		
		this.loadActorData();
		this.updateMovieActorData();
		this.updateRoleData();
		this.updateActorMovieRoleData();
	}
	
	public void startTimer(String str)
	{
    	this.startTime = System.currentTimeMillis();
    	System.out.print(str);
	}
	
	public void stopTimer()
	{
		long duration = System.currentTimeMillis() - this.startTime;
		double elaspe = (double)duration / 1000.0 ;
		System.out.println("Done (" + elaspe + "sec)");
	}
	
	public void RunSampleQuery(String query)
	{
		this.startTimer("Run query:" + query);
		
		SqlLoader tmpSqlLoader = new SqlLoader(0);
		tmpSqlLoader.setInputQuery(query);
		tmpSqlLoader.setUpdateDataType(sqlTableType.SELECT_SQL);
		Thread tmpThread = new Thread(tmpSqlLoader);
		
		try 
		{
			tmpThread.start();
			tmpThread.join();
		} 
		catch (InterruptedException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		this.stopTimer();
	}
	
	public static void main(String[] args)
	{
		// TODO Auto-generated method stub
		System.out.println("Program begin");
		
		SqlEngine engine = new SqlEngine();
		
		String query;
		engine.updateDataToDataBase();
	
		query = 
				"select count(*)\r\n" + 
				"from actor_movie_role\r\n" + 
				"where not exists \r\n" + 
				"(\r\n" + 
				"	select * \r\n" + 
				"	from movie_actor\r\n" + 
				"	where \r\n" + 
				"	actor_movie_role.member_id = movie_actor.member_id and\r\n" + 
				"	actor_movie_role.movie_id = movie_actor.movie_id\r\n" + 
				");";
		engine.RunSampleQuery(query);
		
		query = 
				"select member.member_name\r\n" + 
				"from member join movie_actor \r\n" + 
				"on member.id = movie_actor.member_id \r\n" + 
				"where \r\n" + 
				"member.death_year is null and \r\n" + 
				"member.member_name like 'Phi%' and \r\n" + 
				"member.id not in\r\n" + 
				"(\r\n" + 
				"	select member_id \r\n" + 
				"	from movie_actor join movie on movie_id=movie.id \r\n" + 
				"	where start_year=2014\r\n" + 
				")\r\n" + 
				"group by (member.id);\r\n" + 
				"";
		engine.RunSampleQuery(query);
		
		query = 
				"select member.id, member.member_name \r\n" + 
				"from member \r\n" + 
				"where \r\n" + 
				"member.member_name like '%Gill%' and\r\n" + 
				"member.id in \r\n" + 
				"(\r\n" + 
				"	select movie_producer.member_id \r\n" + 
				"	from movie_producer join movie \r\n" + 
				"	on movie_producer.movie_id = movie.id \r\n" + 
				"	where movie.id in \r\n" + 
				"	(\r\n" + 
				"		select movie_genre.movie_id from movie_genre \r\n" + 
				"		join genre on movie_genre.genre_id = genre.id \r\n" + 
				"		where genre.genre_name=\"Talk-Show\"\r\n" + 
				"	)\r\n" + 
				");\r\n" + 
				"";
		engine.RunSampleQuery(query);
		
		query = 
				"select avg(movie.run_time)\r\n" + 
				"from movie\r\n" + 
				"where movie.original_title like \"%Bhardwaj%\" and\r\n" + 
				"movie.id in\r\n" + 
				"(\r\n" + 
				"	select movie_id \r\n" + 
				"	from movie_writer join member \r\n" + 
				"	on movie_writer.member_id = member.id\r\n" + 
				"	where member.death_year is null\r\n" + 
				");\r\n" + 
				"";
		engine.RunSampleQuery(query);

		query = 
				"select member.id, member.member_name\r\n" + 
				"from member join movie_writer\r\n" + 
				"on member.id = movie_writer.member_id\r\n" + 
				"where \r\n" + 
				"member.death_year is null and\r\n" + 
				"movie_writer.movie_id in\r\n" + 
				"(\r\n" + 
				"	select new_movie_set.id \r\n" + 
				"	from\r\n" + 
				"	(\r\n" + 
				"	select movie.id, COUNT(*) AS magnitude  \r\n" + 
				"	from movie join movie_writer \r\n" + 
				"	on movie.id = movie_writer.movie_id\r\n" + 
				"	where \r\n" + 
				"	movie.run_time > 120\r\n" + 
				"	GROUP BY movie_writer.member_id \r\n" + 
				"	ORDER BY magnitude DESC\r\n" + 
				"	LIMIT 1\r\n" + 
				"	) as new_movie_set\r\n" + 
				");\r\n" + 
				"";
		

		query = 
				"select *\r\n" + 
				"from movie_producer\r\n" + 
				"where \r\n" + 
				"movie_producer.movie_id in\r\n" + 
				"(\r\n" + 
				"	select new_movie_set.id \r\n" + 
				"	from\r\n" + 
				"	(\r\n" + 
				"	select movie.id, COUNT(*) AS magnitude  \r\n" + 
				"	from movie join movie_writer \r\n" + 
				"	on movie.id = movie_writer.movie_id\r\n" + 
				"	where \r\n" + 
				"	movie.run_time > 120\r\n" + 
				"	GROUP BY movie_writer.member_id \r\n" + 
				"	ORDER BY magnitude DESC\r\n" + 
				"	LIMIT 1\r\n" + 
				"	) as new_movie_set\r\n" + 
				");\r\n" + 
				"";
		engine.RunSampleQuery(query);
		
	
		query = 
				"select member.id, member.member_name\r\n" + 
				"from member\r\n" + 
				"where \r\n" + 
				"member.death_year is null and\r\n" + 
				"member.id in\r\n" + 
				"(\r\n" + 
				"select actor_movie_role.member_id \r\n" + 
				"from actor_movie_role join role\r\n" + 
				"on actor_movie_role.role_id = role.id\r\n" + 
				"where\r\n" + 
				"role.role_name like \"%Jesus%\" or \r\n" + 
				"role.role_name like \"%Christ%\"\r\n" + 
				");\r\n" + 
				"";
		engine.RunSampleQuery(query);

		System.out.println("Done");
	}

}
